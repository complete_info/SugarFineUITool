﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using FineUIPro;
 

namespace FineUIPro
{
    public static partial class FineUIProExtensions
    {
 
            /// <summary>
            /// 绑定复选框列表
            /// </summary>
            /// <param name="cbl">控件</param>
            /// <param name="tableName">表名称</param>
            /// <param name="textName">文本字段</param>
            /// <param name="valueName">值字段</param>
            public static void Bind(this FineUIPro.CheckBoxList cbl, string tableName, string textName, string valueName)
            {
                Bind(cbl, tableName, textName, valueName, string.Empty);
            }

            /// <summary>
            /// 绑定复选框列表
            /// </summary>
            /// <param name="cbl">控件</param>
            /// <param name="tableName">表名称</param>
            /// <param name="textName">文本字段</param>
            /// <param name="valueName">值字段</param>
            /// <param name="whereSql">sql条件，注意会有注入危险(不用加where)</param>
            public static void Bind(this FineUIPro.CheckBoxList cbl, string tableName, string textName, string valueName, string whereSql)
            {
                var db = SugarFineUI.DBServices.DB_Base.Instance ;
                string sql = string.Format("select {0},{1} from {2} {3} ", textName, valueName, tableName, (whereSql.Length > 0 ? " where " + whereSql : string.Empty));

                BindByDataSource(cbl, db.Ado.GetDataTable(sql), textName, valueName);
            }

            /// <summary>
            /// 绑定枚举类型到复选框 //Bind(newstype, typeof (EnumClass .NewsType));
            /// </summary>
            /// <param name="cbl"> 复选框</param>
            /// <param name="enumType"> 枚举类型</param>
            public static void Bind(this FineUIPro.CheckBoxList cbl, Type enumType)
            {
                foreach (int i in Enum.GetValues(enumType))
                {
                    cbl.Items.Add(new FineUIPro.CheckItem(Enum.GetName(enumType, i), i.ToString()));
                }
            }

            /// <summary>
            /// 绑定复选框
            /// </summary>
            /// <param name="cbl">控件</param>
            /// <param name="ds">数据源</param>
            /// <param name="textName">文本字段</param>
            /// <param name="valueName">值字段</param>
            public static void BindByDataSource(this FineUIPro.CheckBoxList cbl, object ds, string textName, string valueName)
            {
                cbl.DataTextField = textName;
                cbl.DataValueField = valueName;
                cbl.DataSource = ds;
                cbl.DataBind();
            }

            /// <summary>
            /// 获取选中文本
            /// </summary>
            /// <param name="cbl">复选框</param>
            /// <returns></returns>
            public static string GetTexts(this FineUIPro.CheckBoxList cbl)
            {
                StringBuilder sb = new StringBuilder();
                foreach (FineUIPro.CheckItem item in cbl.SelectedItemArray)
                {
                    sb.AppendFormat("{0},", item.Text);
                }
                return sb.ToString().TrimEnd(',');
            }

            /// <summary>
            /// 获取选中值
            /// </summary>
            /// <param name="cbl">复选框</param>
            /// <returns></returns>
            public static string GetValues(this FineUIPro.CheckBoxList cbl)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string item in cbl.SelectedValueArray)
                {
                    sb.AppendFormat("{0},", item);
                }
                return sb.ToString().TrimEnd(',');
            }
        }
    }


