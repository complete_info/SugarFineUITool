﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace SugarFineUI.Enties
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("sysModule")]
    public partial class sysModule
    {
           public sysModule(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int id {get;set;}

           /// <summary>
           /// Desc:父id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? parentID {get;set;}

           /// <summary>
           /// Desc:页面名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string moduleName {get;set;}

           /// <summary>
           /// Desc:代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string modulePath {get;set;}

           /// <summary>
           /// Desc:地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string moduleUrl {get;set;}

           /// <summary>
           /// Desc:图标地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string moduleIcon {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:1000
           /// Nullable:True
           /// </summary>           
           public int? sort {get;set;}

           /// <summary>
           /// Desc:状态(0不显示页面，1显示在页面)
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? status {get;set;}

           /// <summary>
           /// Desc:备注信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string info {get;set;}

           /// <summary>
           /// Desc:
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public bool? isEnd {get;set;}

    }
}
