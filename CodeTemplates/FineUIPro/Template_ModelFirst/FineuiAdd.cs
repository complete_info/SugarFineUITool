﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlSugar ;

namespace SugarFineUI.CodeTemplates.FineUIPro.Template_ModelFirst
{
    public class FineuiAdd:FineuiBase
    {
        public static string GetCode(TableModel tb)
        {
            string code = string.Format(@"
<%@ Page Language=""C#"" AutoEventWireup=""true"" CodeBehind=""{0}Add.aspx.cs"" Inherits=""{1}.{2}{0}.Add"" ValidateRequest=""false""%>

<!DOCTYPE html>

<html>
<head runat=""server"">
    <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />
    <title></title>
</head>
<body>
    <form id=""form1"" runat=""server"">
        <f:PageManager  runat=""server"" AutoSizePanelID=""form_Edit"" FormLabelWidth=""135px"" />
        <f:Form ID=""form_Edit"" ShowBorder=""false"" ShowHeader=""false"" runat=""server"" AutoScroll=""true""
            EnableCollapse=""true"" BodyPadding=""15px 15px"">
            <Rows>              
 {3}
            </Rows>
            <Toolbars>
                <f:Toolbar  runat=""server"" Position=""Bottom"">
                    <Items>
                        <f:ToolbarFill runat=""server"" />
                        <f:Button ID=""Button_save"" runat=""server"" ValidateForms=""form_Edit"" Text=""保存"" Icon=""SystemSaveNew""
                            OnClick=""Button_save_OnClick"">
                        </f:Button>    
                    </Items>
                </f:Toolbar>
            </Toolbars>
        </f:Form>       
    </form>  
</body>
</html>
", tb.TableName,tb.NamespaceStr, GetNamespace2Str(tb.Namespace2Str), GetFormRowCode(tb.Columns));
            return code;
        }

        private static string GetFormRowCode(List<DbColumnInfo> clms)
        {
            var list= clms.Where(p => p.IsPrimarykey == false).ToList();
            StringBuilder sb=new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                sb.AppendLine("                <f:FormRow runat=\"server\">");
                sb.AppendLine("                     <Items>");
                sb.AppendLine("                         "+ GetFormControl(list[i]));
                sb.AppendLine("                     </Items>");
                sb.AppendLine("                </f:FormRow>");
            }

            return sb.ToString();
        }

        private static string GetFormControl(DbColumnInfo clm)
        {
            https://www.cnblogs.com/wywnet/p/3523941.html 值类型范围

            string showStr = !clm.IsNullable  ? "Required=\"true\"  ShowRedStar=\"true\"" : string.Empty;//是否必填，0不可以为null，1可以为null
            string showName = GetComment(clm.ColumnDescription,clm.DbColumnName);
            string showDefault = clm.DefaultValue.Length == 0 ? string.Empty : clm.DefaultValue.Replace("(", string.Empty).Replace(")", string.Empty);
            switch (clm.DataType.ToLower())
            {
                case "system.boolean":
                    return "<f:CheckBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\"  /> ";

                case "system.byte":
                    return "<f:NumberBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\" " + showStr + " Text=\"" + showDefault + "\" MaxValue=\"255\" MinValue=\"0\" NoDecimal =\"true\" NoNegative=\"true\"></f:NumberBox>";
                case "system.sbyte":
                    return "<f:NumberBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\" " + showStr + " Text=\"" + showDefault + "\" MaxValue=\"127\" MinValue=\"-128\" NoDecimal =\"true\"></f:NumberBox>";
                case "system.int16":
                    return "<f:NumberBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\" " + showStr + " Text=\"" + showDefault + "\" MaxValue=\"32767\" MinValue=\"-32768\" NoDecimal =\"true\"></f:NumberBox>";
                case "system.uint16":
                    return "<f:NumberBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\" " + showStr + " Text=\"" + showDefault + "\" MaxValue=\"65535\" MinValue=\"0\" NoDecimal =\"true\" NoNegative=\"true\"></f:NumberBox>";

                case "system.uint32": //break;
                case "system.uint64": 
                    return "<f:NumberBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\" " + showStr + " Text=\"" + showDefault + "\" MinValue=\"0\" NoDecimal =\"true\" NoNegative=\"true\"></f:NumberBox>";

                case "system.int32":  //break;
                case "system.int64":  
                    return "<f:NumberBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\" " + showStr + " Text=\"" + showDefault + "\"></f:NumberBox> ";

                case "system.decimal"://break;
                case "system.double"://break;
                case "system.single":
                    return "<f:NumberBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName +"\" " + showStr + " Text=\"" + showDefault + "\" NoDecimal =\"false\"></f:NumberBox> ";

                case "system.datetime"://break;
                case "datetime":
                    return "<f:DatePicker ID=\"F_" + clm.DbColumnName + "\" DateFormatString=\"yyyy-MM-dd HH:mm:ss\"  Label=\"" + showName + "\" runat=\"server\" " + showStr + " ></f:DatePicker> ";

                case "system.guid"://break;
                case "system.char"://break;
                case "system.object"://break;
                case "system.string"://break;
                default:
                    return "<f:TextBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\" " + showStr + " ></f:TextBox> ";
            }
        }
        
    }
}
