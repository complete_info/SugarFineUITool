﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlSugar ;

namespace SugarFineUI.CodeTemplates.FineUIPro.Template_ModelFirst
{
    public class FineuiList_cs:FineuiBase
    {
        public static string GetCode(TableModel tb)
        {
            List<DbColumnInfo> list = tb.Columns ;

            DbColumnInfo keyModel = list.FirstOrDefault(p => p.IsPrimarykey == true);
            if (keyModel == null) { throw new Exception("表没有主键，没办法生成代码！FineuiList"); }

            string code = string.Format(@"	
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SqlSugar;
using FineUIPro;
 
namespace {0}.{1}{2}
{{
    public partial class List : {5}
    {{ 
        protected void Page_Load(object sender, EventArgs e)
        {{            
            if (!IsPostBack)
            {{
                InitLoad();
            }}
        }}
       
        private void bind()
        {{
            Grid1.PageSize = int.Parse(ddlPageSize.SelectedValue);

            var db = DBServices.DB_Base.Instance;
            //不要断点此代码，会有委托调试陷阱
            var queryable = db.Queryable<{6}.{2}>()
                //todo 筛选
                //.WhereIF(txb_keyWord.Text.Trim().Length > 0, p => p.Title.Contains(txb_title.Text.Trim()))
                .OrderBy(p => p.{3}, OrderByType.Asc)
                .Select<GridModel>() ;

            Grid1.BindFineuiBySqlSugar(queryable);
        }}   

        #region gird常用方法
        //初始加载
        private void InitLoad()
        {{
            bind();           
        }}


        //搜索
        protected void btnSearch_Click(object sender, EventArgs e)
        {{            
            bind();
        }}

        //删除
        protected void Button_delete_OnClick(object sender, EventArgs e)
        {{
            string ids = GetDataKeysBySelectedRow(Grid1);
            if (ids.Length == 0)
            {{
                NotifyWarning(""请选择记录！"");
                return;
            }}
			DBServices.DB_Base.DeleteByIds<{6}.{2}>(ids);
            bind();
        }}

        //每页显示数目
        protected void ddlPageSize_OnSelectedIndexChanged(object sender, EventArgs e)
        {{
            bind();
        }}

        //分页事件
        protected void Grid1_OnPageIndexChange(object sender, GridPageEventArgs e)
        {{
            Grid1.PageIndex = e.NewPageIndex;
            bind();
        }}    
        
        //关闭window时 刷新grid
        protected void Window1_OnClose(object sender, WindowCloseEventArgs e)
        {{
            bind();
        }}    
        
        public class GridModel
        {{
//todo 自定义显示字段。多表参考http://www.codeisbug.com/Doc/8/1159 五: 返回指定类集合升级版 (//取名为 类名+类中属性名。会自动填充) 
{4}                   
        }}   

        #endregion
        
    }}
}}
", tb.NamespaceStr,
                GetNamespace2Str(tb.Namespace2Str),
                tb.TableName,
                keyModel.DbColumnName,
                GetModelCode(list),
                tb.ClassnameStr,
                tb.ModelName
                ) ;
            return code ;
        }
    }
}
