﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SqlSugar ;

namespace SugarFineUI.CodeTemplates
{
    public class TableModel
    {
        /// <summary>
        /// 数据，表的字段信息
        /// </summary>
        public List<DbColumnInfo> Columns { get; set; }

        /// <summary>
        /// 命名空间
        /// </summary>
        public string NamespaceStr { get; set; }

        /// <summary>
        /// 二级命名空间
        /// </summary>
        public string Namespace2Str { get; set; }

        /// <summary>
        /// 页面继承类名称
        /// </summary>
        public string ClassnameStr { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 实体命名空间名称
        /// </summary>
        public string ModelName { get ; set ; }
    }

    
}
