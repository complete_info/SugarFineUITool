﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SugarFineUI.CodeGenerator.code ;
using FineUIPro ;

namespace SugarFineUI.CodeGenerator.SqlSugar
{
    public partial class SetCode : CodeBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button_save_OnClick(object sender, EventArgs e)
        {
            var db = MyInstance();
            
            try
            {
                db.DbFirst.IsCreateAttribute(true).CreateClassFile(txb_path.Text.Trim(), txb_nameSpace.Text.Trim());
                NotifyInformation("生成代码成功");
            }
            catch (Exception ex)
            {
                NotifyError("生成代码有问题：" + ex.Message) ;
            }
        }
    }
}