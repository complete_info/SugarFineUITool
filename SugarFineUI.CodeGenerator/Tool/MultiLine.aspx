﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiLine.aspx.cs" Inherits="SundayOA.Web.Tool.MultiLine" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>多行文本转成单行文本</title>
</head>
<body>
<form id="form1" runat="server">
    <f:PageManager runat="server" AutoSizePanelID="form_Edit" />

    <f:Form ID="form_Edit" ShowBorder="True" ShowHeader="False" Title=" " runat="server" AutoScroll="true"
            EnableCollapse="false" BodyPadding="15px 15px">
        <Rows>
            <f:FormRow runat="server">
                <Items>
                    <f:TextArea runat="server" ID="txa_old" Height="300px" Required="True" ShowRedStar="True" Label="多行文本"/>
                </Items>
            </f:FormRow>

            <f:FormRow runat="server">
                <Items>
                    <f:TextBox runat="server" ID="txa_new"  Readonly="True" Label="单行文本"/>
                </Items>
            </f:FormRow>

        </Rows>

        <Toolbars>
            <f:Toolbar runat="server" Position="Top">
                <Items>
                    <f:Button runat="server" ValidateForms="form_Edit" ID="btn_showCode" Text="转单行" Icon="Html" OnClick="btn_showCode_OnClick" />
                    <f:ToolbarSeparator runat="server" />
                    <f:CheckBox runat="server" ID="cb_Trim" Label="是否移除每行首尾空字符" Checked="True" LabelAlign="Right"  LabelWidth="175px"/>  
                    <f:ToolbarSeparator runat="server" />
                    <f:ToolbarText runat="server" Text="多行文本转成单行文本"/>
                </Items>
            </f:Toolbar>
        </Toolbars>
    </f:Form>
</form>
</body>
</html>
