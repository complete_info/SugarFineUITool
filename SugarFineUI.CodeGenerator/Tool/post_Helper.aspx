﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="post_Helper.aspx.cs" Inherits="SugarFineUI.CodeGenerator.Tool.ApiHelper.post_Helper" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>参数提交工具</title>
    <style>
        /* 限定列 自动换行，其他列不自动换行 */
        .f-grid-cell.f-grid-cell-Desc .f-grid-cell-inner {
            white-space: normal;
            word-break: break-all;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="panel1" FormLabelWidth="140" FormLabelAlign="Right" />


        <f:Panel ID="panel1" Margin="10px" runat="server" ShowHeader="False" Layout="Region">
            <Items>
                <f:Form runat="server" ID="FormLeftRegion" RegionPosition="Left" RegionSplit="true" EnableCollapse="true"
                    Width="400px" Title="解析报文参数" ShowBorder="true" ShowHeader="true" Layout="Fit"
                    BodyPadding="10px">
                    <Toolbars>

                        <f:Toolbar runat="server" Position="Top">
                            <Items>
                                <f:TextBox runat="server" ID="txb_punctuation" Text="=" Label="键值连接符号" LabelAlign="Right" LabelWidth="110px" Width="160px" />
                                <f:Button runat="server" ID="btn_par" Text="获取参数集合" Icon="Bricks" OnClick="btn_par_OnClick" ValidateForms="FormLeftRegion" />
                            </Items>
                        </f:Toolbar>
                    </Toolbars>

                    <Items>
                        <f:TextArea runat="server" ID="txa_old" Label="获取的参数报文" LabelAlign="Top" Required="True" ShowRedStar="True" />
                    </Items>
                </f:Form>

                <f:Panel runat="server" RegionPosition="Center" ID="FormCenter" ShowBorder="false" ShowHeader="false" Layout="VBox">
                    <Items>
                        <f:Form runat="server" ID="FormGrid" BoxFlex="1" Layout="Fit">

                            <Toolbars>
                                <f:Toolbar runat="server" Position="Top">
                                    <Items>
                                        <f:ToolbarText runat="server" Text="添加自定义参数" />
                                        <f:TextBox runat="server" ID="txb_key" Label="Key" ShowRedStar="True" Required="True" Width="140px" LabelWidth="50px" />
                                        <f:TextBox runat="server" ID="txb_value" Label="Value" Width="140px" LabelWidth="50px" />
                                        <f:Button runat="server" ID="btn_add" Text="添加参数" Icon="Add" OnClick="btn_add_OnClick" ValidateForms="FormGrid" />
                                    </Items>
                                </f:Toolbar>


                            </Toolbars>

                            <Items>

                                <f:Grid ID="Grid1" runat="server"  DataKeyNames="keyStr" IsDatabasePaging="False" Title="要提交的参数集合" EnableTextSelection="True">
                                    <Columns>
                                        <f:TemplateField runat="server" HeaderText="操作" Width="100px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnDel" runat="server" OnCommand="lbtnDel_OnCommand" CommandArgument='<%#Eval("keyStr" )%>'>删除</asp:LinkButton>

                                            </ItemTemplate>
                                        </f:TemplateField>
                                        <f:BoundField runat="server" DataField="keyStr" HeaderText="KEY" Width="150px" />
                                        <f:BoundField runat="server" DataField="valueStr" HeaderText="VALUE" ExpandUnusedSpace="True" ColumnID="Desc" />
                                    </Columns>
                                </f:Grid>
                            </Items>
                        </f:Form>
                        <f:Form runat="server" ID="FormSign" Height="400px" Title="根据url参数示例，生成验签代码" >
                            <Toolbars>
                                <f:Toolbar runat="server" Position="Top">
                                    <Items>
                                        <f:TextBox runat="server" ID="txb_signUrl" Label="示例URL字符串" Width="500px" EmptyText="merchantId=100005&merchantOrderNo=100089282&merchantUserId=hymerchant" Required="True" ShowRedStar="True"/>
                                        <f:Button runat="server" ID="btn_signCode" Text="生成Sign代码"  OnClick="btn_signCode_OnClick" ValidateForms="FormSign"></f:Button>
                                    </Items>
                                </f:Toolbar>                                
                            </Toolbars>
                            <Items>
                                <f:TextArea runat="server" ID="txa_signCode" Label="生成验签代码" Height="300px" Readonly="True" LabelAlign="Top"/>
                            </Items>
                        </f:Form>
                    </Items>


                </f:Panel>

                <f:Form runat="server" ID="FormRightRegion" RegionPosition="Right" RegionSplit="true" EnableCollapse="true"
                    Width="400px" Title="生成POST提交代码" ShowBorder="true" ShowHeader="true" Layout="Fit"
                    BodyPadding="10px">
                    <Toolbars>
                        <f:Toolbar runat="server" Position="Top">
                            <Items>
                                <f:TextBox runat="server" ID="txb_url" Label="要提交的url地址" LabelAlign="Top" Width="340px" Required="True" ShowRedStar="True" Text="http://" />

                            </Items>
                        </f:Toolbar>

                        <f:Toolbar runat="server" Position="Top">
                            <Items>
                                <f:Button runat="server" ValidateForms="FormRightRegion" ID="btn_ParamUrl" Text="生成参数化url" Icon="HtmlGo" OnClick="btn_ParamUrl_OnClick" />
                            </Items>
                        </f:Toolbar>
                        <f:Toolbar runat="server" Position="Top">
                            <Items>
                                <f:TextBox runat="server" ID="txb_Paramurl" Label="参数化url" LabelAlign="Top" Width="340px" Readonly="True" />
                            </Items>
                        </f:Toolbar>

                        <f:Toolbar runat="server" Position="Top">
                            <Items>
                                <f:Button runat="server" ValidateForms="FormRightRegion" ID="btn_post" Text="生成HTML" Icon="HtmlAdd" OnClick="btn_post_OnClick" />
                                <f:Label runat="server" EncodeText="False" Text="<font color=red>注意最后<\/script>  多了个 \ ，自行修改</font>" />
                            </Items>
                        </f:Toolbar>
                    </Toolbars>

                    <Items>
                        <f:TextArea runat="server" ID="txa_html" Label="生成的html" LabelAlign="Top" Readonly="True" />

                    </Items>
                </f:Form>

            </Items>
        </f:Panel>
    </form>
</body>
</html>
<%--<script>
    function replaceValue() {
        var s = F('<%=txa_html.ClientID%>').getValue();
        alert(s);
        F('<%=txa_html.ClientID%>').setValue(s.replace('<\/script>', "<" + "/script>"));
        alert(1);
    }
</script>--%>