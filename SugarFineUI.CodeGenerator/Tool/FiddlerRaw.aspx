﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FiddlerRaw.aspx.cs" Inherits="SugarFineUI.CodeGenerator.Tool.FiddlerRaw" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>根据Fiddler的Raw内容，生成C#请求代码</title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="form_Edit" />

        <f:Form ID="form_Edit" ShowBorder="True" ShowHeader="False" Title=" " runat="server" AutoScroll="true"
                EnableCollapse="false" BodyPadding="15px 15px">
            <Rows>
                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_old" Height="300px" Required="True" ShowRedStar="True" Label="Raw值"/>
                    </Items>
                </f:FormRow>

                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_new"  Height="300px" Readonly="True" Label="生成Code"/>
                    </Items>
                </f:FormRow>  
                
                
                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_SetHeaderValue"  Height="300px" Readonly="True" Label="SetHeaderValue"/>
                    </Items>
                </f:FormRow>

            </Rows>

            <Toolbars>
                <f:Toolbar runat="server" Position="Top">
                    <Items>
                        <f:Button runat="server" ValidateForms="form_Edit" ID="btn_showCode" Text="生成代码" Icon="PageCode" OnClick="btn_showCode_OnClick" />
                        <f:ToolbarSeparator runat="server" />
                        <f:ToolbarText runat="server" Text="根据Fiddler的Raw内容，生成C#请求代码"/>
                    </Items>
                </f:Toolbar>
            </Toolbars>
        </f:Form>
    </form>
</body>
</html>
