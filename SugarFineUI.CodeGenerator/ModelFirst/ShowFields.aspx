﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowFields.aspx.cs" Inherits="SugarFineUI.CodeGenerator.ModelFirst.ShowFields" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>ModelFirst生成字段代码</title>  
    <link href="/Scripts/styles/vs.css" rel="stylesheet" />
</head>
<body>
<script src="/Scripts/highlight.pack.js"></script>
<script>hljs.configure({useBR: true});/*使用code的自动换行，highlight.js不自行增加换行*/</script>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="Panel1" />
        <f:Panel ID="Panel1" CssClass="blockpanel" Margin="5px" runat="server" ShowBorder="false" ShowHeader="false" Layout="Region">
            <items>
                <f:Panel ID="Panel5" runat="server" ShowBorder="True" Layout="VBox" AutoScroll="true" ShowHeader="True" Title="反射DLL的Enties生成字段代码。"
                    BoxConfigChildMargin="0 0 5 0" BodyPadding="5">
                    <items>
                        <f:SimpleForm ID="SimpleForm1"  Height="150px" runat="server"
                            BodyPadding="5px" ShowBorder="true" ShowHeader="false" Layout="Fit" AutoScroll="True">
                            <Toolbars>
                                
                                <f:Toolbar runat="server">
                                    <Items>
                                        <f:TextBox runat="server" ID="txb_Alias" Label="别名" Text="p" Required="True" ShowRedStar="True" LabelWidth="140px"/>
                                        <f:ToolbarSeparator runat="server" />
                                    
                                        <f:Button runat="server" ID="Btn_SetSimpleCode" Text="生成代码" Icon="PageWhiteCsharp" OnClick="Btn_SetSimpleCode_OnClick"  ValidateForms="SimpleForm1" />
                                   </Items>
                                </f:Toolbar>
                            </Toolbars>
                        </f:SimpleForm>
                        <f:TabStrip ID="tab2" BoxFlex="1" Margin="0" IsFluid="true" EnableTabCloseMenu="false"
                            runat="server" BodyPadding="5px" ShowBorder="true">
                            <Tabs>
                                <f:Tab Title="Select字段代码生成" BodyPadding="10px" Layout="Fit" runat="server" AutoScroll="True">
                                    <Items>
                                        <f:Label runat="server" ID="lb_Select" EncodeText="False" />
                                    </Items>
                                </f:Tab>
                                
                            </Tabs>
                        </f:TabStrip>

                    </items>

                </f:Panel> 
                <f:Panel runat="server" ID="panelRightRegion" RegionPosition="Right" RegionSplit="true" EnableCollapse="true" 
                         Width="200px" Title="反射类库" Layout="VBox" ShowBorder="true" ShowHeader="true" BodyPadding="5px" IconFont="_PullRight">
                    <Items>
                        <f:TextBox ID="ttbDB" runat="server" ShowLabel="false" EmptyText="dll的物理地址" ColumnWidth="100%" />
                       
                        <f:Panel runat="server" ShowBorder="False" ShowHeader="False" Height="42px">
                            <Items>
                                <f:Button runat="server" Icon="PageWhiteDatabaseYellow" ID="btn_DBlink" Size="Normal" OnClick="btn_DBlink_OnClick" Text="反射DLL和注释文件"></f:Button>
                            </Items>
                        </f:Panel>
                      
                       
                        <f:RadioButtonList runat="server" ID="rbl_tables" ColumnNumber="1"></f:RadioButtonList>
                    </Items>
                </f:Panel>
             </items>
        </f:Panel>
   
    </form>
</body>
</html>

<script>
    function RefreshTables() {
        __doPostBack('btn_RefreshTables', '');
    }

    function ShowHighlight() {
        $('pre code').each(function(i, block) {
            hljs.highlightBlock(block);
        });
    }
</script>